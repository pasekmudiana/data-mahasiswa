<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h3>Data Mahasiswa</h3>

    <a href="/mahasiswa/tambah"> + Tambah Data + </a>

    <br>
    <br>

    <table border="1">
        <tr>
            <th>Nama</th>
            <th>NIM</th>
            <th>Kelas</th>
            <th>Prodi</th>
            <th>Fakultas</th>
        </tr>

        @foreach ($mahasiswa as $m)
        <tr>
            <td>{{$m->nama_mahasiswa}}</td>
            <td>{{$m->nim_mahasiswa}}</td>
            <td>{{$m->kelas_mahasiswa}}</td>
            <td>{{$m->prodi_mahasiswa}}</td>
            <td>{{$m->fakultas_mahasiswa}}</td>
            <td>
                <a href="/mahasiswa/edit/{{ $m->id }}">Edit</a>
                |
                <a href="/mahasiswa/hapus/{{ $m->id }}">Hapus</a>
            </td>
        </tr>
        @endforeach
    </table>
</body>
</html>