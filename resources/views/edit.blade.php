<!DOCTYPE html>
<html>
    <head>
        <title>CRUD Pada Laravel</title>
    </head>

    <body>
        
        <h3>Edit Data</h3>
              
        
        @foreach($mahasiswa as $s)
        <form action="/mahasiswa/update" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{ $s->id }}"> 
            <br/>
            Nama Mahasiswa <input type="text" required="required" name="nama_mhs" value="{{ $s->nama_mahasiswa }}"> 
            <br/>
            Nim <input type="number" required="required" name="nim_mhs" value="{{ $s->nim_mahasiswa }}"> 
            <br/>
            Kelas <input type="text" required="required" name="kelas_mhs" value="{{ $s->kelas_mahasiswa }}"> 
            <br/>
            Prodi <input type="text" required="required" name="prodi_mhs" value="{{ $s->kelas_mahasiswa }}"> 
            <br/>
            HP <input type="text" required="required" name="fakultas_mhs" value="{{ $s->fakultas_mahasiswa }}"> <br/>
            <input type="submit" value="Simpan Data">
            <br>
            <br>
            <a href="/mahasiswa"> Kembali</a>
        </form>
        
        @endforeach
    </body>
</html>