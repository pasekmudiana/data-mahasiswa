<!DOCTYPE html> 
<html> 
    <head> 
        <title>CRUD Pada Laravel</title> 
    </head> 
    
    <body> 
        
        <h3>Data Mahasiswa</h3> 
              
        <form action="/mahasiswa/store" method="post"> 
            {{ csrf_field() }} 
            Nama Mahasiswa     <input type="text" name="nama_mhs" required="required"> 
            <br /> 
            Nim    <input type="number" name="nim_mhs" required="required"> 
            <br /> 
            Kelas   <input type="text" name="kelas_mhs" required="required"> 
            <br /> 
            Prodi   <input type="text" name="prodi_mhs" required="required"> 
            <br /> 
            Fakultas      <input type="text" name="fakultas_mhs" required="required"> 
            <br /> 
            <input type="submit" value="Simpan Data"> 
            <br>
            <br>
            <a href="/mahasiswa"> Kembali</a> 
        </form> 
    </body> 
</html>